# Torrus.psgi
BEGIN { require '@torrus_config_pl@'; }
use Torrus::PSGI;
my $app = sub { Torrus::PSGI->run_psgi(@_) };
