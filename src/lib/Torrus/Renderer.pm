#  Copyright (C) 2002-2011  Stanislav Sinyagin
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

# Stanislav Sinyagin <ssinyagin@yahoo.com>

package Torrus::Renderer;
use strict;
use warnings;

use Digest::MD5 qw(md5_hex);
use File::Spec;
use Template;

use Torrus::DB;
use Torrus::ConfigTree;
use Torrus::TimeStamp;
use Torrus::RPN;
use Torrus::Log;
use Torrus::SiteConfig;
use Torrus::Renderer::Cache;

# Inherit methods from these modules
use base qw(Torrus::Renderer::HTML
            Torrus::Renderer::RRDtool
            Torrus::Renderer::Frontpage
            Torrus::Renderer::AdmInfo
            Torrus::Renderer::RPC);

our $VERSION = 1.0;


=head1 METHODS

=head2 new

Creates the new object and returns it

=cut

sub new
{
    my $class = shift;
    my $self = {};
    bless $self, $class;

    my $cache = Torrus::Renderer::Cache->new();
    $self->{cache} = $cache;

    $self->{'tt'} =
        Template->new(INCLUDE_PATH => $Torrus::Global::templateDirs,
                    TRIM => 1);

    return $self
}

=head2 render

Returns the absolute filename and MIME type:

 my($fname, $mimetype) = $renderer->render($config_tree, $token, $view);

=cut

sub render
{
    my $self = shift;
    my $config_tree = shift;
    my $token = shift;
    my $view = shift;
    my %new_options = @_;
    my $cache = $self->{cache};

    # If no options given, preserve the existing ones
    if( %new_options )
    {
        $self->{'options'} = \%new_options;
    }

    $cache->checkAndClearCache( $config_tree );

    my($t_render, $t_expires, $content, $mime_type);

    my $tree = $config_tree->treeName();

    if( not $config_tree->isTset($token) )
    {
        if( my $alias = $config_tree->isAlias($token) )
        {
            $token = $alias;
        }
        if( not defined( $config_tree->path($token) ) )
        {
            Error("No such token: $token");
            return
        }
    }

    $view = $config_tree->getDefaultView($token) unless defined $view;

    my $uid = $self->{'options'}->{'uid'} || '';

    my $cachekey = $uid . ':' . $tree . ':' . $token . ':' . $view;

    ($t_render, $t_expires, $content, $mime_type) =
        $cache->getCache( $cachekey, $self->{options} );

    # Hit! return it!
    return ($content, $mime_type, $t_expires - time())
        if $t_render;

    my $method = 'render_' . $config_tree->getParam($view, 'view-type');

    ($t_expires, $mime_type, $content) =
        $self->$method( $config_tree, $token, $view );

    if( %new_options )
    {
        $self->{'options'} = undef;
    }

    if( defined($t_expires) and defined($mime_type) )
    {
        $cache->setCache($cachekey, $self->{options}, time(), $t_expires, $content, $mime_type);
        return ($content, $mime_type, $t_expires - time());
    }

    return
}

=head2 xmlnormalize($txt)

normalizes I<$txt> and returns it

=cut

sub xmlnormalize
{
    my $txt = shift;

    # Remove spaces in the head and tail.
    $txt =~ s/^\s+//om;
    $txt =~ s/\s+$//om;

    # Unscreen special characters
    $txt =~ s/{COLON}/:/ogm;
    $txt =~ s/{SEMICOL}/;/ogm;
    $txt =~ s/{PERCENT}/%/ogm;

    $txt =~ s/\&/\&amp\;/ogm;
    $txt =~ s/\</\&lt\;/ogm;
    $txt =~ s/\>/\&gt\;/ogm;
    $txt =~ s/\'/\&apos\;/ogm;
    $txt =~ s/\"/\&quot\;/ogm;

    return $txt
}



1;


# Local Variables:
# mode: perl
# indent-tabs-mode: nil
# perl-indent-level: 4
# End:
