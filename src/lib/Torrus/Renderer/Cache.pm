#  Copyright (C) 2012  Dean Hamstead
#  Copyright (C) 2002-2011  Stanislav Sinyagin
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

# Dean Hamstead <dean@fragfest.com.au>
# Stanislav Sinyagin <ssinyagin@yahoo.com>


package Torrus::Renderer::Cache;
use strict;
use warnings;

use Digest::MD5 qw(md5_hex);
use Cache::FastMmap;
use FreezeThaw qw(freeze thaw);

use Torrus::DB;
use Torrus::TimeStamp;
use Torrus::Log;

our $VERSION = 1.0;

sub new
{
    my $class = shift;
    my $self = {};
    bless $self, $class;

    $self->{store} = Cache::FastMmap->new(
                expire_time => $Torrus::Renderer::cacheMaxAge || 60,
    );

    die 'Couldnt create cache object?' unless $self->{store};

    return $self
}

=head2 cacheKey($keystring, [\%options])

needs a unique string I<$keystring>. If the I<\%options> hashref
is provided it will be used as well to generate the I<$newkeystring>
that is returned

=cut

sub cacheKey
{
    my $self = shift;
    my $keystring = shift;
    my $options = shift || {};

    if( ref( $options->{'variables'} ) )
    {
        for my $name ( sort keys %{$options->{'variables'}} )
        {
            my $val = $options->{'variables'}->{$name};
            $keystring .= ':' . $name . '=' . $val;
        }
    }
    return $keystring
}


=head2 getCache($keystring)

needs I<$keystring> as a parameter, returns the cache values as a I<@list>

=cut

sub getCache
{
    my $self = shift;
    my $keystring = shift;
    my $options = shift || {};

    $keystring = $self->cacheKey($keystring,$options);

    my $cacheval = $self->{store}->get( $keystring );

    if( defined($cacheval) )
    {
        my $o = thaw( $cacheval );

        return @{$o}[qw(t_render t_expires content mime_type)]
           if ($o->{t_expires} >= time());

        # otherwise we go to the end and return nothing

    }

    #return ($t_render, $t_expires, $content, $mime_type)
    return (0, 0, '', '')

}

=head2 setCache($keystring, $options, $t_render, $t_expires, $filename, $mime_type)

Sets a value in the cache based on the provided arguments

=cut

sub setCache
{
    my $self = shift;
    my $keystring = shift;
    my $options = shift;
    my $t_render = shift;
    my $t_expires = shift;
    my $content = shift;
    my $mime_type = shift;

    $keystring = $self->cacheKey($keystring,$options);

    my $o = {
        t_render   => $t_render,
        t_expires  => $t_expires,
        content    => $content,
        $mime_type => $mime_type,

    };

    $self->{store}->set( $keystring, freeze($o) );

    return 1
}

=head2 checkAndClearCache

Stub.

=cut

sub checkAndClearCache
{
return 1
}

=head2 clearcache

Stub.

=cut

sub clearcache
{
    return 1
}

1;


# Local Variables:
# mode: perl
# indent-tabs-mode: nil
# perl-indent-level: 4
# End:
