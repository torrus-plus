#  Copyright (C) 2002  Stanislav Sinyagin
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

# $Id: DB.pm,v 1.1 2012/01/08 23:58:23 root Exp root $
# Stanislav Sinyagin <ssinyagin@yahoo.com>
# Dean Hamstead <dean@fragfest.com.au>

package Torrus::DB;

use strict;
use warnings;

use Torrus::Log;

our $VERSION = 1.0;

our $dbType; # this is set in torrus-config.pl or torrus-siteconfig.pl

sub new
{
    my $c = shift;
    my @args = @_;

    my $location = "Torrus/DB/${dbType}.pm";
    my $class = "Torrus::DB::$dbType";

    require $location;

    return $class->new(@args)
}


# It is strongly inadvisable to do anything inside a signal handler when DB
# operation is in progress

our $interrupted = 0;

my $signalHandlersSet = 0;
my $safeSignals = 0;





sub setSignalHandlers
{
    if( $signalHandlersSet )
    {
        return;
    }

    $SIG{'TERM'} = sub {
        if( $safeSignals )
        {
            Warn('Received SIGTERM. Scheduling to exit.');
            $interrupted = 1;
        }
        else
        {
            Warn('Received SIGTERM. Stopping the process.');
            exit(1);
        }
    };

    $SIG{'INT'} = sub {
        if( $safeSignals )
        {
            Warn('Received SIGINT. Scheduling to exit.');
            $interrupted = 1;
        }
        else
        {
            Warn('Received SIGINT. Stopping the process');
            exit(1);
        }
    };


    $SIG{'PIPE'} = sub {
        if( $safeSignals )
        {
            Warn('Received SIGPIPE. Scheduling to exit.');
            $interrupted = 1;
        }
        else
        {
            Warn('Received SIGPIPE. Stopping the process');
            exit(1);
        }
    };

    $SIG{'QUIT'} = sub {
        if( $safeSignals )
        {
            Warn('Received SIGQUIT. Scheduling to exit.');
            $interrupted = 1;
        }
        else
        {
            Warn('Received SIGQUIT. Stopping the process');
            exit(1);
        }
    };

    $signalHandlersSet = 1;
    return;
}


sub setSafeSignalHandlers
{
    setSignalHandlers();
    $safeSignals = 1;
    return;
}


sub setUnsafeSignalHandlers
{
    setSignalHandlers();
    $safeSignals = 0;
    return;
}

=head2 checkInterrupted

If we were previously interrupted, gracefully exit now

=cut

sub checkInterrupted
{
    if( $interrupted )
    {
        Warn('Stopping the process');
        exit(1);
    }
    return;
}



sub closeNow
{
die 'This should be implemented in the DB/Foo.pm package';
}

sub cleanupEnvironment
{
    # this is pretty crappy but will do for now
    # a better approach would be to use the objects and let them
    # clean themselves when they are destroyed
    for my $janitor (@Torrus::DB::cleanUp)
    {
        $janitor->();
    }
    return

}


sub delay
{
    my $self = shift;
    $self->{'delay_list_commit'} = 1;
    return
}


sub trunc
{
die 'This should be implemented in the DB/Foo.pm package';
}


sub put
{
die 'This should be implemented in the DB/Foo.pm package';
}

sub get
{
die 'This should be implemented in the DB/Foo.pm package';
}


sub del
{
die 'This should be implemented in the DB/Foo.pm package';
}


sub cursor
{
die 'This should be implemented in the DB/Foo.pm package';
}


sub next
{
die 'This should be implemented in the DB/Foo.pm package';
}

sub c_del
{
die 'This should be implemented in the DB/Foo.pm package';
}


sub c_get
{
die 'This should be implemented in the DB/Foo.pm package';
}

sub c_put
{
die 'This should be implemented in the DB/Foo.pm package';
}


sub c_close
{
die 'This should be implemented in the DB/Foo.pm package';
}


sub getBestMatch
{
die 'This should be implemented in the DB/Foo.pm package';
}


sub searchPrefix
{
die 'This should be implemented in the DB/Foo.pm package';
}


sub searchSubstring
{
die 'This should be implemented in the DB/Foo.pm package';
}


sub addToList
{
die 'This should be implemented in the DB/Foo.pm package';
}


sub searchList
{
die 'This should be implemented in the DB/Foo.pm package';
}


sub delFromList
{
die 'This should be implemented in the DB/Foo.pm package';
}


sub getListItems
{
die 'This should be implemented in the DB/Foo.pm package';
}



sub deleteList
{
die 'This should be implemented in the DB/Foo.pm package';
}


sub commit
{
die 'This should be implemented in the DB/Foo.pm package';
}



1;


# Local Variables:
# mode: perl
# indent-tabs-mode: nil
# perl-indent-level: 4
# End:
