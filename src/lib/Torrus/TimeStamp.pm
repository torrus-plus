#  Copyright (C) 2002-2011  Stanislav Sinyagin
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

# Stanislav Sinyagin <ssinyagin@yahoo.com>
# Dean Hamstead <dean@fragfest.com.au>

package Torrus::TimeStamp;

use strict;
use warnings;

use Torrus::DB;
use Torrus::Log;

our $VERSION = 1.1;

=head1 NAME

Torrus::TimeStamp - TimeStamp object for Torrus

=head1 SYNOPSIS

    use Torrus::TimeStamp;
    my $obj = Torrus::TimeStamp->new();
    my $ret = $obj->setNow($tname);
    my $ret = $obj->get($tname);
    my $ret = $obj->release();

=head1 DESCRIPTION

This moudule provides a TimeStamp object that quickly sets and gets
timestamps based on a provided timestamp name ie I<$tname>

=head1 METHODS

=head2 new

Creates the object

=cut

sub new
{
    my $c = shift;
    my $self = {};
    bless $self, $c;
    $self->{db} = Torrus::DB->new('timestamps', -WriteAccess => 1);
    return $self
}

=head2 release

Releases the underlying database, could probably be dropped as
DESTROY will do the same job.

=cut

sub release
{
    my $self = shift;
    delete $self->{db}
        if $self->{db};
    return 1
}

=head2 setNow($tname)

Records a timestamp called I<$tname> to the current time.

=cut

sub setNow
{
    my $self = shift;
    my $tname = shift;
    $self->{db}->put( $tname, time() );
    return 1;
}

=head2 get($tname)

Retrieves a timestamp named I<$tname>, or returns 0 if not found.

=cut

sub get
{
    my $self = shift;
    my $tname = shift;
    my $stamp = $self->{db}->get( $tname );
    return defined($stamp) ? $stamp : 0;
}

sub DESTROY
{

    my $self = shift;
    delete $self->{db}
        if $self->{db};

}

=head1 CREDITS

Dean Hamsted rewrote the original version by SS

=head1 AUTHOR

As per B<CREDITS>

=cut

1;

# Local Variables:
# mode: perl
# indent-tabs-mode: nil
# perl-indent-level: 4
# End:
