#!/usr/bin/perl
package Torrus::PSGI;

use strict;
use warnings;

# because Miyagawa knows much more than SS about perl, and Ive written mod_perl1/2/fcgi adapters enough times now in my life
use Plack::Request;
use Plack::Response;

use JSON ();

use Torrus::Log;
use Torrus::Renderer;
use Torrus::SiteConfig;
use Torrus::ACL;

our $VERSION = 0.1;

=head2 run_psgi

The outmost function, can probably be collapsed down in to I<do_process>

=cut

sub run_psgi
{

    my $c = shift; # class name

    my $env = shift; # PSGI env

    my $q = Plack::Request->new($env);

    my $res = __PACKAGE__->do_process($q);

    # this is a travesty
    &Torrus::DB::cleanupEnvironment();

    return $res->finalize

}

=head2 do_process

Processes the web request itself

=cut

sub do_process
{

    my $c = shift;

    my $q = shift;

    my $path_info = $q->path_info;

    my @paramNames = $q->param();

    &Torrus::Log::setLevel('debug')
        if( $q->param('DEBUG') and not $Torrus::Renderer::globalDebug );

    my %options;
    for my $name ( @paramNames )
    {
	next if $name eq 'SESSION_ID';
        $options{'variables'}->{$name} = $q->param($name)
            if $name =~ m/^[A-Z]/;
    }

    my( $content, $mimetype, $expires );
    my %cookies;

    my $renderer = Torrus::Renderer->new();
    if( not defined( $renderer ) )
    {
        return report_error($q, 'Error initializing Renderer');
    }

    my $tree = _path_to_tree($q);

    _determine_uid($q,\%options,\%cookies);

    if( not $content )
    {
        if( not $tree or not Torrus::SiteConfig::treeExists( $tree ) )
        {
            ( $content, $mimetype, $expires ) =
                $renderer->renderTreeChooser( %options );
        }
        else
        {
            if( $Torrus::CGI::authorizeUsers and
                not $options{'acl'}->hasPrivilege( $options{'uid'}, $tree,
                                                   'DisplayTree' ) )
            {
                return report_error($q, 'Permission denied');
            }

            if( $Torrus::Renderer::displayReports and
                defined( $q->param('htmlreport') ) )
            {
#                if( $Torrus::CGI::authorizeUsers and
#                    not $options{'acl'}->hasPrivilege( $options{'uid'}, $tree,
#                                                       'DisplayReports' ) )
#                {
#                    return report_error($q, 'Permission denied');
#                }
#
#                my $reportfname = $q->param('htmlreport');
#                # strip off leading slashes for security
#                $reportfname =~ s/^.*\///o;
#
#                $fname = $Torrus::Global::reportsDir . '/' . $tree .
#                    '/html/' . $reportfname;
#
#                return report_error($q, 'No such file: ' . $reportfname)
#                    if not -f $fname;
#
#                $mimetype = 'text/html';
#                $expires = '3600';
            }
            else
            {
                my $config_tree = Torrus::ConfigTree->new( -TreeName => $tree );
                return report_error($q, 'Configuration is not ready')
                    if not defined $config_tree ;

                my $token = $q->param('token');
                if( not defined($token) )
                {
                    my $path = $q->param('path');
                    if( not defined($path) )
                    {
                        my $nodeid = $q->param('nodeid');
                        if( defined($nodeid) )
                        {
                            $token = $config_tree->getNodeByNodeid( $nodeid );
                            return report_error($q, 'Cannot find nodeid: ' . $nodeid)
                                if not defined $token
                        }
                        else
                        {
                            $token = $config_tree->token('/');
                        }
                    }
                    else
                    {
                        $token = $config_tree->token($path);
                        return report_error($q, 'Invalid path')
                            if not defined $token
                    }
                }
                elsif( $token !~ m/^S/ and
                       not defined( $config_tree->path( $token ) ) )
                {
                    return report_error($q, 'Invalid token')
                }

                my $view = $q->param('view');
                $view = $q->param('v')
                   if not defined $view;

                return report_error($q, 'Invalid view name: ' . $view)
                    if( defined $view and not $config_tree->viewExists($view) );

                ( $content, $mimetype, $expires ) =
                    $renderer->render( $config_tree, $token, $view, %options );

                undef $config_tree;
            }
        }
    }

    &Torrus::DB::cleanupEnvironment();

    if( defined( $options{'acl'} ) )
    {
        undef $options{'acl'};
    }

    if( defined($content) )
    {

        Debug("Render returned |$content| |$mimetype| |$expires|");

        my $res = Plack::Response->new(200,{'Content-type' => $mimetype, 'Expires' => $expires,});
        $res->cookies(%cookies);
        $res->body($content);
        return $res

    }
    else
    {
        return report_error($q, "Renderer returned error.\n" .
                            "Probably wrong directory permissions or " .
                            "directory missing:\n" )
    }

#    if( not $Torrus::Renderer::globalDebug )
#    {
#        &Torrus::Log::setLevel('info');
#    }

    return;

}


sub report_error
{
    my $q = shift;
    my $msg = shift;

    my $v = $q->param('view');
    if( defined $v and $v eq 'rpc' )
    {
        my $json = JSON->new();
        $json->pretty;
        $json->canonical;
	my $res = Plack::Response->new(200,{'Content-Type' => 'application/json', 'Expires' => 'now'});
        $res->body($json->encode({'success' => 0, 'error' => $msg}));
	return $res
    }
    else
    {
        my $res = Plack::Response->new(500,{'Content-type' => 'text/plain', 'Expires' => 'now'});
        $res->body('Error: ' . $msg);
	return $res
    }

    return
}

sub _determine_uid
{

    my $q = shift;
    my $options = shift;
    my $cookies = shift;

    return 1 unless $Torrus::CGI::authorizeUsers;

    my $uid;

        $options->{acl} = Torrus::ACL->new();

        my $hostauth = $q->param('hostauth');
        if( defined( $hostauth ) )
        {
            $uid = $q->address();
            $uid =~ s/\W/_/go;
            my $password = $uid . '//' . $hostauth;

            Debug('Host-based authentication for ' . $uid);

            if( not $options->{acl}->authenticateUser( $uid, $password ) )
            {
		my $res = Plack::Response->new(403,{ 'Content-Type' => 'text/plain' });
                $res->body('Host-based authentication failed for ' . $uid);
                Info('Host-based authentication failed for ' . $uid);
                return $res
            }

            Info('Host authenticated: ' . $uid);
            $options->{uid} = $uid;
        }
        else
        {

            my $ses_id = $q->cookies->{'SESSION_ID'};

            my $needs_new_session = 1;
            my %session;

            if( $ses_id )
            {
                # create a session object based on the cookie we got from the
                # browser, or a new session if we got no cookie
                my $eval_ret = eval
                {
                    tie %session, 'Apache::Session::File', $ses_id, {
                        Directory     => $Torrus::Global::sesStoreDir,
                        LockDirectory => $Torrus::Global::sesLockDir }
                };
                if( $eval_ret and not $@ )
                {
                    if( $options->{'variables'}->{'LOGOUT'} )
                    {
                        tied( %session )->delete();
                    }
                    else
                    {
                        $needs_new_session = 0;
                    }
                }
            }

            if( $needs_new_session )
            {
                tie %session, 'Apache::Session::File', undef, {
                    Directory     => $Torrus::Global::sesStoreDir,
                    LockDirectory => $Torrus::Global::sesLockDir };
            }

            # might be a new session, so lets give them their cookie back

            my %cookie = (name  => 'SESSION_ID',
                          value => $session{'_session_id'});
            $cookies->{SESSION_ID} = \%cookie;

            if( $session{'uid'} )
            {
                $options->{'uid'} = $session{'uid'};
                if( $session{'remember_login'} )
                {
                    $cookie{'expires'} = time + 24 * 60 * 60 * 60;
                }
            }
            else
            {
                my $needsLogin = 1;

                # POST form parameters

                $uid = $q->param('uid');
                my $password = $q->param('password');
                if( defined( $uid ) and defined( $password ) )
                {
                    if( $options->{acl}->authenticateUser( $uid, $password ) )
                    {
                        $session{'uid'} = $options->{uid} = $uid;
                        $needsLogin = 0;
                        Info('User logged in: ' . $uid);

                        if( $q->param('remember') )
                        {
                            $cookie{'expires'} = time + 24 * 60 * 60 * 60;
                            $session{'remember_login'} = 1;
                        }
                    }
                    else
                    {
                        $options->{'authFailed'} = 1;
                    }
                }

#                if( $needsLogin )
#                {
#                    $options->{'urlPassTree'} = __PACKAGE__->_path_to_tree($q);
#                    for my $param ( 'token', 'path', 'nodeid',
#                                        'view', 'v' )
#                    {
#                        my $val = $q->param( $param );
#                        $options->{'urlPassParams'}{$param} = $val
#                            if defined $val and $val ne ''
#                    }
#
#                    ( $content, $mimetype, $expires ) =
#                        $renderer->renderUserLogin( %$options );
#
#                    die('renderUserLogin returned undef') unless $content;
#                }
            }
            untie %session;

        }

    return $uid

}

sub _path_to_tree {

    my $q = shift;

    my $tree = $q->path_info;
    $tree =~ s/^.*\/(.*)$/$1/;

    return $tree

}


1;

# Local Variables:
# mode: perl
# indent-tabs-mode: nil
# perl-indent-level: 4
# End:
