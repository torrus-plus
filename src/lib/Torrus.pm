package Torrus;
# $Id $

use strict;
use warnings;

our $VERSION = 2.01;

1;

__END__

=head1 NAME

Torrus - A perl based monitoring platform aimed at large installations

=head1 DESCRIPTION

Torrus is an monitoring platform similar to MRTG, Cricket and Cacti. In most cases it brings more flexibility, scalability and performance. It has been able to poll 1 million SNMP OIDs every 5 minutes from a modern server.

Torrus is designed to be the universal data series processing framework. Its scalable hierarchical design, application-independent core, and highly customizable architecture make Torrus an attractive choice both for small installations and for big enterprise or carrier networks.

Although users typically deploy Torrus for SNMP monitoring, it can equally be used for the graphing of data series' of any nature.

Tobi Oetiker's RRDtool is used for data storage.

=head1 VERSION

2.01

=head1 AUTHOR

Stanislav Sinyagin

Additional contributors as per the I<AUTHORS> file

=head1 LICENSE AND COPYRIGHT

Torrus is free software. It is distributed under the GNU General Public License v2. Full license is described in the I<COPYING> file
