#!/usr/bin/perl


use Test::More qw/no_plan/;
use Data::Dumper qw/Dumper/;

use strict;
use warnings;

BEGIN { require_ok('/opt/torrus/conf_defaults/torrus-config.pl'); };
BEGIN { use_ok('Torrus::DB'); };
BEGIN { use_ok('Torrus::Log'); };


my $debug = 1;

sub Log;
*Log = \&Torrus::Log::Log;
{
    no warnings;
    *Torrus::Log::Log = sub { };
}

sub debug
{
    my ($options, $got, $expected) = @_;

    print STDERR
            "options = ", Dumper($options),
            "got = ", Dumper($got),
            "expected = ", Dumper($expected)
        if $debug;
}

sub randNum
{
    my $limit = shift || 10000;

    return int( rand($limit) );
}

sub by_keypair
{
    return ( $a->[0] cmp $b->[0] || $a->[1] cmp $b->[1] );
}

sub diff_pair
{
    my ($a,$b) = @_;
    return ( $a->[0] cmp $b->[0] || $a->[1] cmp $b->[1] );
}

sub dbName
{
    my %options = @_;
    my $prefix = $options{'prefix'} || 'testdb';

    return $prefix
            . '-'
            . ( $options{'-Btree'} ? 'btree' : 'hash' )
            . '-'
            . ( $options{'-Duplicates'} ? 'dup-' : '' )
            . &randNum();
}

sub db_slurp
{
    my $db = shift or return;
    my %options = @_;

    my $cursor = $db->cursor();
    my @db_values;
    while ( my ($k,$v) = $db->next($cursor) )
    {
        push @db_values, [$k,$v];
    }
    #print "slurp = ", Dumper(\@db_values);

    @db_values = sort by_keypair @db_values;

    return wantarray ? @db_values : \@db_values;
}

sub db_put
{
    my ($db, @values) = @_;
    $db->put(@$_) for @values;
}

sub db_get
{
    my ($db, @values) = @_;
    my @results = map { $db->get(@$_) } @values;

    return wantarray ? @results : \@results;
}

sub db_open
{
    my ($dbname, %options) = @_;

    my $dbh = new Torrus::DB( $dbname, %options );

    #print "db_open = ", Dumper($dbh);
    if ( $dbh and ref $dbh and $dbh->{'dbh'} )
    {
        return $dbh;
    }
    else
    {
        return;
    }
}

sub db_del
{
    my ($db, $key) = @_;

    $db->del($key);
}


sub expected
{
    my ($options, @values) = @_;

    my @expected;

    # if duplicates keys are supported, keep pairs with same key but different values
    if ( $options->{'-Duplicates'} )
    {
        foreach my $pair ( @values )
        {
            unless ( grep { !diff_pair($_,$pair) } @expected )
            {
                push @expected, $pair;
            }
        }
    }
    else    # hash/Btree without duplicates
    {
        my %map;
        for ( @values )
        {
            my ($k,$v) = @$_;
            $map{$k} = $v;
        }
        for ( @values )
        {
            my ($k,$v) = @$_;
            if ( defined $map{$k} and $map{$k} eq $v )
            {
                push @expected, [ $k, $v ];
                delete $map{$k};
            }
        }
    }

    @expected = sort by_keypair @expected;

    return wantarray ? @expected : \@expected;
}



# check DB home directory exists
my $dbHome = $Torrus::Global::dbHome;
ok( $dbHome && -d $dbHome, '$Torrus::Global::dbHome defined and exists' );


# default options for DB creation
my %options;
my %dbs_delete;
my $db_subdir = 'filers';



#
# check database doesn't already exist - we'll create it
#

#{
#    my %options = ( -Subdir => 0, -Btree => 0, -WriteAccess => 0, -Duplicates => 0 );
#    my $dbname = &dbName( %options );
#
#    my $db = db_open( $dbname, %options );
#    ok( !defined($db), "db does not exist ($dbname)" );
#
#}







#
# create new database
#


{
    my %options = ( -Subdir => 0, -Btree => 0, -WriteAccess => 1, -Duplicates => 0 );
    my $dbname = &dbName( %options );

    my $db = db_open( $dbname, %options );
    ok( $db, "create hash database ($dbname)" );

    $dbs_delete{ $db->{'dbname'} } ++;
}




#
# close db handle
#

{
    my %options = ( -Subdir => $db_subdir, -Btree => 0, -WriteAccess => 1, -Duplicates => 0 );
    my $dbname = &dbName( %options );

    my $db = db_open( $dbname, %options );
    $db->closeNow();
    ok( !defined $db->{'dbh'}, "gracefully close database handle ($dbname)" );

    $dbs_delete{ $db->{'dbname'} } ++;
}



#
# create new database, then try to create it again - should get a cached db handler ref
#

#{
#    my %options = ( -Subdir => $db_subdir, -Btree => 0, -WriteAccess => 1, -Duplicates => 0 );
#    my $dbname = &dbName( %options );
#
#    my $db = db_open( $dbname, %options );
#    #print "db = ", Dumper($db);
#
#    ok( $db->{'dbname'} =~ m#$db_subdir#, "database file found in subdir ($dbname)" );
#
#    # checked for cached DB handle
#    my $db2 = db_open( $dbname, %options );
#    is_deeply($db, $db2, "existing db handle cached ($dbname)");
#
#    $dbs_delete{ $db->{'dbname'} } ++;
#    $dbs_delete{ $db2->{'dbname'} } ++;
#}




#
#
#

{
    my %options = ( -Subdir => $db_subdir, -Btree => 0, -WriteAccess => 1, -Duplicates => 1 );
    my $dbname = &dbName( %options );

    my $db = db_open( $dbname, %options );

    #print"\n\nWriting values to database ($dbname) using \$db->put()\n\n";
    my ($key1,$val1) = (5,0);
    my ($key2,$val2) = (9,undef);
    my @values = ( [1,2], [1,3], [2,4], [5,10], [1,1], [2,6], [$key1,$val1], [$key2,$val2] );
    db_put( $db, @values );

    my $value;

    $value = $db->get( $key1 );
    ok( $value == $val1, 'insert and retrieve non-NULL values' );

    $value = $db->get( $key2 );
    ok( $value eq '', 'insert and retrieve NULL values' );

    $db->del( $key1 );
    $value = $db->get( $key1 );
    ok( !defined $value, 'retrieve deleted key' );

    $db->trunc();
}




#
# Cursoring: db->next, cursor get (c_get) / cursor put (c_put) / cursor del (c_del)
#

{
    my %options = ( -Subdir => $db_subdir, -Btree => 0, -WriteAccess => 1, -Duplicates => 1 );

    foreach my $cfg (
            { -Btree => 0, -Duplicates => 0 },
            { -Btree => 0, -Duplicates => 1 },
            { -Btree => 1, -Duplicates => 0 },
            { -Btree => 1, -Duplicates => 1 },
    )
    {
        @options{ keys %$cfg } = values %$cfg;
        #print "options: ", Dumper( \%options );

        my $dbname = &dbName( %options );
        my @values = ( [1,2], [1,3], [2,4], [5,10], [1,1], [2,6], [5,0] );

        my $db = db_open( $dbname, %options );
        db_put( $db, @values );



        # cursor iteration
        my $cursor = $db->cursor( -Write => 1 );

        my @db_values;
        #print "iterating using next()\n";
        while ( my ($k,$v) = $db->next($cursor) )
        {
            #print "$k => $v\n";
            push @db_values, [$k,$v];
        }

        my @got = sort by_keypair @db_values;
        my @expected = expected( \%options, @values );

        is_deeply( \@got, \@expected, "verifying cursor iteration ($dbname)" )
            || debug( \%options, \@got, \@expected);



        # c_put - add values to db using cursor
        my ($key,$val) = @{$values[0]};
        $val += 100;
        $db->c_put( $cursor, $key => $val );

        @got = db_slurp( $db, %options );
        @expected = expected( \%options, @values, [$key,$val] );

        is_deeply( \@got, \@expected, "read value from db with c_get ($dbname)" )
            || debug( \%options, \@got, \@expected);

        $dbs_delete{ $db->{'dbname'} } ++;



        # c_get tests
        if ( !$options{'-Duplicates'} )
        {
            my $v = $db->c_get($cursor, $key);
            ok( $v == $val, "c_get ($key) on db without duplicates ($dbname), got($v) wanted($val)" );
        }




        # c_del - deletion with cursor
        $db->c_del( $cursor );

        @got = db_slurp( $db, %options );

        #print "got = ", Dumper(\@got);

        # if duplicates are allowed, then we only need to remove the key/value pair we just added
        # but we never added it to @values, so @values is what should be in the DB
        if ( $options{'-Duplicates'} )
        {
            @expected = expected(\%options, @values);
        }
        # otherwise, remove all pairs with key == $key from @values, because no duplicates are allowed,
        # and the one key/value pair with key == $key, val = $val was removed by c_del();
        else
        {
            @expected = grep { $_->[0] != $key && $_->[1] != $val } expected(\%options, @values);
        }

        #print "db_values:\n", Dumper( \@got );
        is_deeply( \@got, \@expected, "cursor delete key ($key) values with c_del ($dbname)" )
            || debug( \%options, \@got, \@expected);

        $db->trunc();
    }
}






#
# truncate database
#

{
    my %options = ( -Subdir => $db_subdir, -Btree => 0, -WriteAccess => 1, -Duplicates => 0 );
    my $dbname = &dbName( %options );
    my @values = ( [1,2], [1,3], [2,4], [5,10], [1,1], [2,6], [5,0] );

    my $db = db_open( $dbname, %options );

    db_put( $db, @values );
    ok( $db->trunc(), "truncating database ($dbname)" );

    my @got = db_slurp($db, %options);
    my @expected = ();

    is_deeply( \@got, \@expected, "verifying truncated db ($dbname)" )
        || debug( \%options, \@got, \@expected);

    $db->trunc();
    $dbs_delete{ $db->{'dbname'} } ++;
}





#
# open HASH database and get a handle. check hash does not support duplicates
#

{
    my %options = ( -Subdir => $db_subdir, -Btree => 0, -WriteAccess => 1, -Duplicates => 0 );
    my $dbname = &dbName( %options );
    my @values = ( [1,2], [1,3], [2,4], [5,10], [1,1], [2,6], [5,0] );

    my $db = db_open( $dbname, %options );

    db_put( $db, @values );
    my @got = db_slurp($db, %options);
    my @expected = expected(\%options, @values);

    is_deeply( \@got, \@expected, "writing to, then reading from hash db ($dbname)" )
        || debug( \%options, \@got, \@expected);

    $db->trunc();
    $dbs_delete{ $db->{'dbname'} } ++;
}






#
# create Btree db without duplicate support. add data with duplicates and verify
#

{
    my %options = ( -Subdir => $db_subdir, -Btree => 1, -WriteAccess => 1, -Duplicates => 0 );
    my $dbname = &dbName( %options );
    my @values = ( [1,2], [1,3], [1,4], [1,3], [10,20], [10,30] );

    my $db = db_open( $dbname, %options );

    db_put( $db, @values );
    my @got = db_slurp($db, %options);
    my @expected = expected(\%options, @values);

    is_deeply( \@got, \@expected, "Btree db without duplicates ($dbname)" )
        || debug( \%options, \@got, \@expected);

    $db->trunc();
    $dbs_delete{ $db->{'dbname'} } ++;
}



# tested to here


#
# create Btree db WITH duplicate support. add data with duplicates and verify
#

{
    my %options = ( -Subdir => $db_subdir, -Btree => 1, -WriteAccess => 1, -Duplicates => 1 );
    my $dbname = &dbName( %options );
    my @values = ( [1,2], [1,3], [1,4], [1,3], [10,20], [10,30] );

    my $db = db_open( $dbname, %options );

    db_put( $db, @values );

    my @got = db_slurp($db, %options);
    my @expected = expected(\%options, @values);

    is_deeply( \@got, \@expected, "Btree db supports duplicates ($dbname)" )
        || debug( \%options, \@got, \@expected);

    $db->trunc();
    $dbs_delete{ $db->{'dbname'} } ++;
}





#
# create Btree db with duplicates. Delete data (by key) and verify.
#

{
    my %options = ( -Subdir => $db_subdir, -Btree => 1, -WriteAccess => 1, -Duplicates => 1 );
    my $dbname = &dbName( %options );
    my @values = ( [1,2], [1,3], [1,4], [1,3], [10,20], [10,30] );

    my $db = db_open( $dbname, %options );
    db_put( $db, @values );

    my $key = $values[0][0];
    $db->del( $key );
    my @db_values = db_slurp($db);

    # remove deleted values
    my @got = sort by_keypair @db_values;
    my @expected = grep { $_->[0] != $key } expected(\%options, @values);

    is_deeply( \@got, \@expected, "Btree delete confirmed ($dbname)" )
        || debug( \%options, \@got, \@expected);

    $db->trunc();
    $dbs_delete{ $db->{'dbname'} } ++;
}





#
# iterate over db with cursor. if using a Btree db, expect results to be sorted
#


{
    my %options = ( -Subdir => $db_subdir, -Btree => 0, -WriteAccess => 1, -Duplicates => 0 );

    foreach my $cfg (
            { -Btree => 0, -Duplicates => 0 },
            { -Btree => 0, -Duplicates => 1 },
            { -Btree => 1, -Duplicates => 0 },
            { -Btree => 1, -Duplicates => 1 }, )
    {
        @options{ keys %$cfg } = values %$cfg;

        my $dbtype = $options{'-Btree'} ? 'Btree' : 'HASH';
        my $dup = $options{'-Duplicates'} ? ' with duplicates' : '';

        my $dbname = &dbName( %options );
        my @values = ( [50,5], [30,3], [90,9], [20,2], [80,8], [10,1], [30,30] );

        my $db = db_open( $dbname, %options );
        db_put($db, @values);

        my @got = db_slurp($db, %options);
        my @expected = expected(\%options, @values);

        is_deeply( \@got, \@expected, "cursor iteration for $dbtype" . $dup . " ($dbname)" )
           || debug( \%options, \@got, \@expected);

        $db->trunc();
        $dbs_delete{ $db->{'dbname'} } ++;
    }
}




#
# close db handle
#

{
    my %options = ( -Subdir => $db_subdir, -Btree => 0, -WriteAccess => 1, -Duplicates => 0 );
    my $dbname = &dbName( %options );
    my @values = ( [1,2], [1,3], [1,4], [1,3], [10,20], [10,30] );

    my $db = db_open( $dbname, %options );
    db_put( $db, @values );

    $db->trunc();
    $dbs_delete{ $db->{'dbname'} } ++;
}






#
# searching - getBestMatch
#

{
    my %options = ( -Subdir => $db_subdir, -Btree => 1, -WriteAccess => 1, -Duplicates => 1 );
    my $dbname = &dbName( %options );
    my @values = ( ['a',0], ['abc',2], ['abcd',3], ['abcdef',5], ['abcdefg',6], ['abcdefgh',7], ['x',9], ['z',9] );

    sub _search_test
    {
	my ($dbh, $searchKey, $expected, $dbname, $options, $desc) = @_;

	my $got = $dbh->getBestMatch( $searchKey );

	is_deeply( $got, $expected, "searching with getBestMatch ($dbname) - $desc" )
	    || debug( $options, $got, $expected);
    };


    my $db = db_open( $dbname, %options );
    db_put( $db, @values );

    my ($key,$value) = @{$values[0]};
    my $searchKey;

    $searchKey = $key;
    _search_test( $db, $searchKey, { exact => 1 }, $dbname, \%options, "exact match ($searchKey)" );

    $searchKey = 'ab';
    _search_test( $db, $searchKey, { key => $key, value => $value }, $dbname, \%options,
								"prefix match ($searchKey)" );

    $searchKey = '1111x';
    _search_test( $db, $searchKey, undef, $dbname, \%options, "failed search ($searchKey)" );


    # add duplicate values and test again
    my $newval = $value + 10;
    my $longkey = 'abcdef';

    push @values, [$longkey, $newval];
    db_put( $db,  [$longkey, $newval]);

    $searchKey = $longkey;
    _search_test( $db, $searchKey, { exact => 1 }, $dbname, \%options, 'exact match with duplicates' );



    # expect to match on shorter key
    $key = 'abcd'; $value = 3;
    chop ( $searchKey = $longkey );
    _search_test( $db, $searchKey, { key => $key, value => $value }, $dbname, \%options, 'shortest key' );

    $searchKey = $longkey;
    _search_test( $db, $searchKey, { exact => 1 }, $dbname, \%options,
                                    "exact match on longer key ($searchKey)" );

    my @a = db_slurp( $db, %options );
    #print "db values: ", Dumper(\@a);

    $searchKey = $longkey . $longkey;
    _search_test( $db, $searchKey, { key => 'abcdef', value => 5 },
                                    $dbname, \%options, "long key ($searchKey)" );



    $db->trunc();
    $dbs_delete{ $db->{'dbname'} } ++;

    #undef $db;

}






#
# searchPrefix
#
{
    my %options = ( -Subdir => $db_subdir, -Btree => 1, -WriteAccess => 1, -Duplicates => 1 );
    my $dbname = &dbName( %options );
    my $a_len = 4;
    my $b_len = $a_len * 2;
    my ($string, $got, @got, @expected);

    my @values;
    push @values, ( ['a',1], ['aa',2], ['aaa',3], ['aaaa',4], ['aaaaa',5] );
    push @values, ( ['bbbbbbbb',0], ['bbbbbbb',1], ['bbbbbb',2], ['bbbbb',3], ['bbbb',4] );

    my $db = db_open( $dbname, %options );
    db_put( $db, @values );


    # search over sorted list
    $string = 'aaa';
    $got = $db->searchPrefix( $string );
    @got = ( defined $got ? sort by_keypair @$got : (undef) );
    @expected = grep { $_->[0] =~ $string } expected(\%options, @values);

    is_deeply( \@got, \@expected, "searchPrefix sorted list ($dbname)" )
        || debug( \%options, \@got, \@expected);


    # search over unsorted list
    $got = $db->searchPrefix('b' x ($b_len/2) );
    @got = ( defined $got ? sort by_keypair @$got : () );

    @expected = expected(\%options, @values);
    @expected = sort by_keypair
                    grep { $_->[0] =~ 'b' && length($_->[0]) >= $b_len/2 }
                        @expected;

    is_deeply( \@got, \@expected, "searchPrefix unsorted list ($dbname)" )
        || debug( \%options, \@got, \@expected);


    # search for non-existent key
    $got = $db->searchPrefix( 'c' );
    @got = ( defined $got ? sort by_keypair @$got : (undef) );
    @expected = (undef);

    is_deeply( \@got, \@expected, "searchPrefix non-existent key ($dbname)" )
        || debug( \%options, \@got, \@expected);

    $db->trunc();
    $dbs_delete{ $db->{'dbname'} } ++;
}




#
# searchSubstring
#
{
    my %options = ( -Subdir => $db_subdir, -Btree => 0, -WriteAccess => 1, -Duplicates => 1 );
    my $dbname = &dbName( %options );
    my $a_len = 4;
    my $b_len = $a_len * 2;
    my ($string, $got, @got, @expected);

    my @values;
    push @values, map { ['abbba' x $_, $_ ] } 1..$a_len;
    push @values, map { ['cddeeec' x ($b_len-$_+1), $_ ] } 1..$b_len;

    my $db = db_open( $dbname, %options );
    db_put( $db, @values );


    # search over sorted list
    $string = 'bbaabb';
    $got = $db->searchSubstring( $string );
    @got = ( defined $got ? sort by_keypair @$got : (undef) );
    @expected = grep { $_->[0] =~ $string } expected(\%options, @values);

    is_deeply( \@got, \@expected, "searchSubstring sorted list ($dbname)" )
        || debug( \%options, \@got, \@expected);

    # another search, where the order is not guaranteed
    $string = 'eeccdd';
    $got = $db->searchSubstring( $string );
    @got = ( defined $got ? sort by_keypair @$got : (undef) );
    @expected = grep { $_->[0] =~ $string } expected(\%options, @values);

    is_deeply( \@got, \@expected, "searchSubstring unsorted list ($dbname)" )
        || debug( \%options, \@got, \@expected);

    # search for non-existent key
    $got = $db->searchSubstring( 'ZZZZ' );
    @got = ( defined $got ? sort by_keypair @$got : (undef) );
    @expected = (undef);

    is_deeply( \@got, \@expected, "searchSubstring non-existent substring ($dbname)" )
        || debug( \%options, \@got, \@expected);

    $db->trunc();
    $dbs_delete{ $db->{'dbname'} } ++;
}





#
# list functions - addToList, searchList, delFromList, deleteList
#

{
    sub _verify_lists
    {
        my $db = shift;
        my @values = @_;

        #print "_verify_lists\n";
        my $ok = 1;
        for ( @values )
        {
            my ($list,$val) = @$_;
            foreach my $var ( split /,/, $val )
            {
                $ok = $db->searchList($list, $var) ? $ok : 0;
                #print "search for $list: $ok\n";
            }
        }
        return $ok;
    }

    my %options = ( -Subdir => $db_subdir, -Btree => 0, -WriteAccess => 1, -Duplicates => 0 );
    my $dbname = &dbName( %options );

    my @values = map { [ 'list'.$_, join(',',1..$_) ] } 1..10;

    my $db = db_open( $dbname, %options );
    db_put( $db, @values );

    my $ok;
    $ok = _verify_lists($db, @values);
    ok( $ok, "searchList - verify list creation ($dbname)" );


    my $list = 'list4';
    my $val = 'test';

    $ok = $db->addToList( $list, $val );
    ok( $ok, "addToList - new number ($dbname)" );


    $ok = _verify_lists($db, [$list,$val] );
    ok( $ok, "searchList - verify new number added ($dbname)" );


    $ok = $db->addToList( $list, $val );
    ok( $ok, "addToList - add number again ($dbname)" );


    $ok = $db->addToList( $list, $val, 1 ); # force unique test - should fail
    ok( !$ok, "addToList - don't add if already exists ($dbname)" );


    $ok = $db->searchList( $list, $val );
    ok( $ok, "searchList - search list for value($dbname)" );


    $ok = $db->searchList( $list, 'non-existent value' );
    ok( !$ok, "searchList - non-existent value ($dbname)" );


    $ok = $db->delFromList( $list, $val );
    ok( $ok, "delFromList - delete pair($dbname)" );


    $ok = _verify_lists($db, @values);
    ok( $ok, "searchList - verify remaining lists ($dbname)" );


    my $pair = shift @values;
    ($list,$val) = @$pair;

    $ok = $db->deleteList( $list );
    ok( $ok, "deleteList ($list) api call ($dbname)" );


    $ok = _verify_lists($db, @values);
    ok( $ok, "deleteList ($list) verify ($dbname)" );

    #$db->{'dbh'}->commit();

    $db->trunc();
    #$db->{'dbh'}->commit();

    $dbs_delete{ $db->{'dbname'} } ++;
}







#
# cleanup - remove database files that were created by this test
#
{

    my $ok = 1;
    for ( grep { -f $_ } map { $dbHome . '/' . $_ } keys %dbs_delete )
    {
        $ok = ( unlink($_) ? $ok : 0 );
    }
    ok( $ok, 'database file cleanup' );

}

