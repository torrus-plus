NOTICE
======

This repository contains additions to Torrus that I am publishing as a courtesy to the original author as I am not required to do so by the softwares' license. He may or may not commit them to the mainline Torrus software in the future (probably not), so if you use them - please take this in to consideration.

The goals of my changes are therefore my own and, to be clear, i have no intention of creating a fully fledged fork of the software. You might compare this then, to the well known patch sets against the Linux kernel.

All my changes are licensed under the GPLv2, which is the same license as the original software when I branched (or when i merged subsequent upstream changes). As such, you must acknowledge me as an author of my work if you use it elsewhere. I have satisfied clause 2a of the GPLv2 via the Changelog file. In the case of future license changes to my work, of course I must also be consulted as is my right according to clause 10.  Of course, there is no warranty given or implied with this software.

You are of course, welcomed and encouraged to fork and merge my code at your distretion, respecting the GPLv2 license.

Major Differences
-----------------

 * WIP - The modperl1/2/fastcgi has been replaced with Plack. So you can not connect it to anything that talks PSGI, including  modperl1/2/fastcgi, but also native psgi webservers/containers and Torrus can now run as a stand alone webapp (no apache etc).

 * WIP - Uses Module::Install, rather than c-like / homegrown Makefile stuff.

 * WIP - Caching uses Cache::FastMmap rather than a homegrown write to files with bdb index thing.

 * There is much stronger seperation of concerns.

 * Ive tried to reduce the cross namespace dancing that the original author uses so liberally.

 * What i have written, the Perl style is modern and looks like perl rather than java mixed with c.

 * New web theme

Help Me
-------
If you have issues with software from this repository, please don't contact the original author.

Contact Me
----------
Please only contact me via GitHub's in built tools. You can quickly and easily add coments to code, raise bugs, send patches etc.

Where can I get MIB files?
--------------------------
Many vendors don't make their MIB files publicly available, though dont seem to mind if they are mirrored on the internet. That said, there is not a clear license for many (most) of them. There are a number of websites that make MIB files available, though many of them are dated. The Netdisco project maintains a tarball of MIB files which they have fixed up (as vendors often provide broken MIB files) via their website http://www.netdisco.org, I recommend this as a good place to start.

Original README content
-----------------------
Torrus.org source code

See Torrus Wiki for more details on Git repositories:
https://sourceforge.net/apps/mediawiki/torrus
